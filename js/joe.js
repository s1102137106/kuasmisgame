//自訂函數
var Joe = {
	format:function() {
		if (arguments.length == 0){
			return null;
		}
		var str = arguments[0];
		for ( var i = 1; i < arguments.length; i++) {
			var re = new RegExp('\\{' + (i - 1) + '\\}', 'gm');
			str = str.replace(re, arguments[i]);
		}
		return str;
	},

	getLeft:function(id) {
		var str_left = $("#" + id).css("left");
		var strarr_left = str_left.split("p");
		return parseInt(strarr_left[0]);
	},


	getTop:function(id) {
		var str_top = $("#" + id).css("top");
		var strarr_top = str_top.split("p");
		return parseInt(strarr_top[0]);
	},


	getWidth:function(id) {
		var str_width = $("#" + id).css("width");
		var strarr_width = str_width.split("p");
		return parseInt(strarr_width[0]);
	},


	getHeight:function(id) {
		var str_height = $("#" + id).css("height");
		var strarr_height = str_height.split("p");
		return parseInt(strarr_height[0]);
	}


};
