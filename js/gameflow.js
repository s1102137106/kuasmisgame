var me;
function GameFlow() {
    me = this;
    $(document).ready(function () {
        me.initialMenu();
    });
}
//設定遊戲的欄位
GameFlow.prototype = {
    int_armscount: 0,//主角攻擊次數
    int_beeLP: 9,//主角的血量
    int_monsterLP: 10,//王的血量
    int_monsterLPGroup: 5,//一個血量要打五次
    int_boosAttackCount: 0,//王的攻擊次數
    int_littleMonsterAttackCount: 0,//小怪發射數量
    int_boos1SkillCount: 0,//第一關第一小王技能
    isGameExist: false,//遊戲開始
    firstGame: true,//第一次玩
    isBossExist: false,//王是否已經出現
    isVistory: false,//打贏還是打輸
    GameTime: 1,//玩到第幾階段 
    int_audioCount: 0,//音樂的數量
    isBoosAttackDio: false,//第一次大招提示
    isBoosAttackDio2: false,//第二次大招提示
    int_destoryLittleMonster: 0,//摧毀的小怪數
    int_littleBossLP: 0,//小怪的血量
    int_maxGameTime: 1//解鎖關卡
};

//初始化選單
GameFlow.prototype.initialMenu = function () {
    var audio = Joe.format("<audio autoplay loop id='menuMusic'  class='audioStart' > </audio>");
    $("#c").append(audio);
    var source = Joe.format(" <source src = 'audio/{0}.mp3' type = 'audio/mpeg'>", "menu");
    $("#menuMusic").append(source);

    $("#start").click(me.choseLevel);
    $("#explanation").click(me.explanationGame);
    $("#about").click(me.aboutGame);

    //加入滑鼠移動特效
    joinMouseHover("start");
    joinMouseHover("explanation");
    joinMouseHover("about");

}

//破關後選單更新
GameFlow.prototype.proMenu = function () {
    var changeImgId = ["start", "explanation", "about"];//要改變的圖片id
    for (var i = 0; i < changeImgId.length; i++) {
        $("#" + changeImgId[i]).remove();//移除原照片
        var startImg = Joe.format("<img id='{0}' class='{0}' src='sourceimg/{0}.png' style='width:100%;height:100%;float:left;position: absolute'>", "final_" + changeImgId[i]);
        $("#" + changeImgId[i] + "Div").append(startImg);
        joinMouseHover("final_" + changeImgId[i]);
        $("#" + changeImgId[i] + "Div").css({
            width: "14%",
            height: "12%"
        });
    }
    $("#title").attr("src", Joe.format("sourceimg/{0}.png", "final_title"));
    $("#titleDiv").css({
        left: "33%",
    });
    $("#final_start").click(me.choseLevel);
    $("#final_explanation").click(me.explanationGame);
    $("#final_about").click(me.aboutGame);
}

//游標滑過特效-更換圖片&指標變pointer
joinMouseHover = function (id) {
    $("#" + id).hover(function () {
        $("#" + id).attr("src", Joe.format("sourceimg/{0}_touch.png", id));
    },
            function () {
                $("#" + id).attr("src", Joe.format("sourceimg/{0}.png", id));
            });
    $("#" + id).css({
        cursor: "pointer"
    });
}

//選擇關卡
GameFlow.prototype.choseLevel = function () {
    var choseDiv = "<div id=\"choseDiv\" class=\"about2Div\"></div>";
    $("#c").append(choseDiv);
    var aboutcloseimg = "<img id=\"aboutcloseimg\" style=\"position: absolute;width:14%;height:9%;left:74%;top:75%\" src=\"sourceimg/close.png\" >";
    $("#choseDiv").append(aboutcloseimg);
    $("#choseDiv").css("background-image", "url(\"sourceimg/level/chose/level_bk.png\")");
    joinClossImg();
    for (var i = 1; i <= 4; i++) {
        var left = 13 + 22 * (i - 1);
        var levelDiv = Joe.format("<div id='levelDiv{0}'  class='levelDiv' style='position: absolute;width:11%;height:20%;left:{1}%;top:13%' ></div>", i, left); //遊戲關卡div
        $("#choseDiv").append(levelDiv);
        var levelfont = Joe.format("<img id='levelfont{0}'  class='levelImg' style='position: absolute;width:100%;height:20%;left:0%;top:80%'  src='sourceimg//level/chose/level_{0}_font.png\' >", i);
        $("#levelDiv" + i).append(levelfont);
        if (me.int_maxGameTime >= i) {
            var levelimg = Joe.format("<img id='level{0}'  class='levelImg' value='{0}' style='position: absolute;width:100%;height:70%;left:0%;top:0%'  src='sourceimg//level/chose/level_{0}_pic.png\' >", i);
            $("#levelDiv" + i).append(levelimg);
            joinMouseHoverShadow("levelDiv" + i);
            $("#level" + i).click(me.startGame);
            $("#levelfont" + i).click(me.startGame);
        } else {
            var levelimg = Joe.format("<img id='level{0}'  class='levelImg' value='{0}' style='position: absolute;width:100%;height:70%;left:0%;top:0%'  src='sourceimg//level/chose/level_{0}_pic_lock.png\' >", i);
            $("#levelDiv" + i).append(levelimg);
        }

        $("#levelDiv" + i).css({
            cursor: "pointer"
        });
    }
    $("#aboutcloseimg").click(function () {
        $("#choseDiv").remove();
    });
}

//游標滑過特效-陰影&指標變pointer
joinMouseHoverShadow = function (id) {
    $("#" + id).hover(function () {
        $("#" + id).css({
            '-webkit-box-shadow': '0 0 60px #f3d42e',
            '-moz-box-shadow': '0 0 60px #f3d42e',
            'box-shadow': '0 0 60px #f3d42e',
        });
    },
            function () {
                $("#" + id).css({
                    '-webkit-box-shadow': '',
                    '-moz-box-shadow': '',
                    'box-shadow': '',
                });
            });

}

//遊戲開始
GameFlow.prototype.startGame = function () {
    if (typeof ($(this).attr("value")) != "undefined") {
        me.GameTime = parseInt($(this).attr("value"));//進入關卡
    }
    $("#choseDiv").remove();
    $("#menuMusic").remove();
    me.clearAllAudio();
    me.isGameExist = true;
    $("#c").append(start2Div);    //隱藏選單
    $("#menu").css({
        "display": "none"
    });

    var start2Div = Joe.format("<div id='start2Div'  class='start2Div'></div>"); //遊戲總介面
    $("#c").append(start2Div);

    me.joinAudio("one_p");//加入遊戲背景音樂

    if (me.GameTime == 1 || me.GameTime == 3) { //增加開場白只有第一關跟第三關
        me.joinGameDioDiv("sourceimg/level/start_font.png");
        var windowWidth = $(window).width();
        var startGameDioSpeed = 13; //每次往右移13
        var t_startGameDio = window.setInterval(function startGameDio() {
            var left = getLeft("startGameDio") + startGameDioSpeed;
            if (windowWidth > getLeft("startGameDio")) {
                $("#startGameDio").css({//
                    "left": left + "px"
                });
            } else {//game start
                $("#startGameDioDiv").remove();
                window.clearInterval(t_startGameDio);
                me.joinAllElement();
                return false;
            }
        }, 20);

    } else {
        me.joinAllElement();
    }
}

//加入遊戲角色和資訊
GameFlow.prototype.joinAllElement = function () {
    //加入小怪
    me.joinLittleMonster();

    //加入主角
    me.joinMainBee();

    //加入資訊
    me.joinInfo();

    //控制主角&發射攻擊
    if (me.firstGame) {
        $(document).keydown(me.moveMain);
        $(document).keyup(me.starArms);
    } else {
        $(document).on("keydown", me.moveMain);
        $(document).on("keyup", me.starArms);
    }

    //第一次玩
    me.firstGame = false;

    //設定時間加入期中考以及該關boss
    var t = setTimeout("examination()", 3000);
}

//遊戲說明
GameFlow.prototype.explanationGame = function () {
    var about2Div = "<div id=\"about2Div\" class=\"about2Div\"></div>";
    $("#c").append(about2Div);

    var aboutcloseimg = "<img id=\"aboutcloseimg\" style=\"position: absolute;width:14%;height:9%;left:74%;top:75%\" src=\"sourceimg/close.png\" >";
    $("#about2Div").append(aboutcloseimg);
    $("#about2Div").css("background-image", "url(\"sourceimg/info.png\")");

    joinClossImg();

    $("#aboutcloseimg").click(function () {
        $("#about2Div").remove();
    });
}

//遊戲製作團隊
GameFlow.prototype.aboutGame = function () {
    var about2Div = "<div id=\"about2Div\" class=\"about2Div\"></div>";
    $("#c").append(about2Div);
    var aboutcloseimg = "<img id=\"aboutcloseimg\" style=\"position: absolute;width:14%;height:9%;left:74%;top:75%\" src=\"sourceimg/close.png\" >";
    $("#about2Div").append(aboutcloseimg);

    joinClossImg();
    $("#about2Div").css("background-image", "url(\"sourceimg/about2.png\")");
    $("#aboutcloseimg").click(function () {
        $("#about2Div").remove();
    });
}

//加入關閉扭特效
joinClossImg = function () {
    $("#aboutcloseimg").hover(function () {
        $("#aboutcloseimg").attr("src", "sourceimg/close_touch.png");
    },
           function () {
               $("#aboutcloseimg").attr("src", "sourceimg/close.png");
           });

    $("#aboutcloseimg").css({
        cursor: "pointer"
    });
}

//技能飛行(boss)
GameFlow.prototype.boosAttackMove = function () {
    /*/攻擊的資訊
     type=0 littleMonster各種書
     type=1 boss
     type=2 boss1大絕招
     type=3 boos2 大絕招謝老師
     type=4 boos3 大絕招
     type=5 boos4大絕招1問號
     type=6 boos4大絕招2蛋蛋 
    */
    var temp = $(this).attr("value");
    var temp2 = temp.split("|");


    var attackId = temp2[0];    //攻擊物件的id
    var boosAttackMoveRange = temp2[1];    //王的攻擊飛行速度
    var type = temp2[2];//王的攻擊種類
    var mainbeeTop = getTop("mainbeeDiv");
    var up = true;
    var frequency = 100;
    var moveCount = 0;
    var angle = 0;
    var first = true;
    var t_moveAttackBoos = window.setInterval(function moveAttackBoos() {
        //判斷有沒有攻擊到主角 
        if (!me.isGameExist) {
            $("#" + attackId).remove();
            window.clearInterval(t_moveAttackBoos);
            return false;
        }
        if (getWidth("mainbeeDiv") + getLeft("mainbeeDiv") > getLeft(attackId) && getHeight(attackId) + getTop(attackId) > getTop("mainbeeDiv") && getTop(attackId) < getTop("mainbeeDiv") + getHeight("mainbeeDiv") && getLeft(attackId) + getWidth(attackId) > getLeft("mainbeeDiv")) {
            $("#" + attackId).remove();
            //依照被攻擊的type來決定扣幾滴血
            if (type == "0" || type == "1") {
                me.int_beeLP = me.int_beeLP - 1;
            } else if (type == "2") {
                me.int_beeLP = me.int_beeLP - 2;
            } else if (type == "3") {//加一 受到王的攻擊扣的血量在這邊
                me.int_beeLP = me.int_beeLP - 1;
            } else if (type == "4") {
                me.int_beeLP = me.int_beeLP - 1;
            } else if (type == "5") {//問號攻擊
                me.int_beeLP = me.int_beeLP - 2;
            } else if (type == "6") {//蛋蛋攻擊
                me.int_beeLP = me.int_beeLP - 1;
            }
            if (me.int_beeLP < 1) {
                $("#mainbeeBlood").attr("src", "sourceimg/player/HP/hp1.png");
                $("#infoBlood").attr("src", "sourceimg/player/HP/hp1.png");
            } else if (me.int_beeLP >= 1 && me.int_beeLP <= 9) {
                $("#mainbeeBlood").attr("src", "sourceimg/player/HP/hp" + (me.int_beeLP + 1) + ".png");
                $("#infoBlood").attr("src", "sourceimg/player/HP/hp" + (me.int_beeLP + 1) + ".png");
            } else {
                $("#mainbeeBlood").attr("src", "sourceimg/player/HP/hp10.png");
                $("#infoBlood").attr("src", "sourceimg/player/HP/hp10.png");
            }

            window.clearInterval(t_moveAttackBoos); //被攻擊到 此攻擊就可消失
            return false;
        }
        //判斷主角生死
        if (me.int_beeLP < 1) {
            $(document).off("keydown", me.moveMain);
            $(document).off("keyup", me.starArms);

            $("#start2Div").remove();
            me.isGameExist = false;
            me.endGameMessage();
        } else {
            //如果跑出螢幕左邊就將他移除
            if (getLeft(attackId) < 0) {
                $("#" + attackId).remove();
                window.clearInterval(t_moveAttackBoos);
                return false;
            }
            moveCount = moveCount + 1;
            //攻擊模式
            if (type == "0" || type == "1") {
                var tempLeft = getLeft(attackId) - boosAttackMoveRange;
                $("#" + attackId).css({//
                    "left": tempLeft + "px",
                });
            } else if (type == "2") {//天受的coffee小追蹤
                var tempLeft = getLeft(attackId) - boosAttackMoveRange;
                if (getTop(attackId) > mainbeeTop) {
                    var tempTop = getTop(attackId) - 2;
                } else {
                    var tempTop = getTop(attackId) + 2;
                }

                $("#" + attackId).css({//
                    "left": tempLeft + "px",
                    "top": tempTop + "px",
                });
            } else if (type == "3") {//文川 
                parseFloat(boosAttackMoveRange)
                boosAttackMoveRange = parseFloat(boosAttackMoveRange) + 0.05;
                var tempLeft = getLeft(attackId) - boosAttackMoveRange;
                $("#" + attackId).css({//
                    "left": tempLeft + "px",
                });
            } else if (type == "4") {//郝老師的leave
                var tempTop = getTop(attackId);
                var tempLeft = getLeft(attackId) - boosAttackMoveRange;
                if (moveCount % frequency == 0) {
                    if (up) {
                        up = false;
                    } else {
                        up = true;
                    }
                    moveCount = 0;
                }

                if (up) {
                    $("#" + attackId).css({//
                        "left": tempLeft + "px",
                        "top": (tempTop + 4) + "px",
                    });
                } else {
                    $("#" + attackId).css({//
                        "left": tempLeft + "px",
                        "top": (tempTop - 8) + "px",
                    });
                }
            } else if (type == "5") {
                var tempLeft = getLeft(attackId) - boosAttackMoveRange;
                var tempTop = $(window).height() / 2 - (getHeight(attackId) / 2);
                $("#" + attackId).css({//
                    "left": tempLeft + "px",
                    "top": tempTop + "px",
                });
            } else if (type == "6") {
                if (first) {
                    setInterval(function () {
                        if (!me.isGameExist) {
                            $("#" + attackId).remove();
                            return false;
                        }
                        angle += 3;
                        $("#" + attackId).rotate(angle);
                    }, 10);
                    first = false;
                }
                var tempTop = 0;
                var moveTop = 8;
                //撞到牆
                if (getTop(attackId) - 2 < 0) {
                    up = false;
                    //     moveTop = moveTop + 2;
                } else if (getTop(attackId) + getHeight(attackId) + 2 > windowHeight) {
                    //    moveTop = moveTop + 2;
                    up = true;
                }
                if (up) {
                    tempTop = getTop(attackId) - moveTop;
                } else {
                    tempTop = getTop(attackId) + moveTop;
                }
                var tempLeft = getLeft(attackId) - boosAttackMoveRange;
                $("#" + attackId).css({//
                    "left": tempLeft + "px",
                    "top": tempTop + "px",
                });
            }
        }
    }, 10);
}

//發射技能(玩家)
GameFlow.prototype.starArms = function (e) {
    var keycode = e.which || e.keyCode;
    var str_left = $("#mainbeeDiv").css("left");
    var strarr_left = str_left.split("p");
    var str_top = $("#mainbeeDiv").css("top");
    var strarr_top = str_top.split("p");
    if (keycode == 32) {//space
        //武器數量
        me.int_armscount = me.int_armscount + 1;
        var topp = parseInt(strarr_top[0]);
        var lefte = parseInt(strarr_left[0]) + 50;
        var arms = "<img id=\"arms" + me.int_armscount + "\" value=\"" + me.int_armscount + "\" class=\"arms\" style=\"position: absolute;width:5%;height:5%;top:" + topp + "px;left:" + lefte + "px\" src=\"sourceimg/zzz.png\" >";
        $("#c").append(arms);
        $("#arms" + me.int_armscount).load(me.armsMove);
    }
}

//攻擊資訊的物件
var bossAttackInfo = {
    frequency: 0,//新增攻擊頻率
    moveRange: 0,//攻擊的速度
    type: "1",//攻擊的種類
    name: "coffee",//圖片名稱
    style: { height: "0%", width: "0%" }//圖片大小
};

//技能飛行(玩家)
GameFlow.prototype.armsMove = function (e) {
    var str_value = $(this).attr("value");
    var armTop = getTop("mainbeeDiv");
    var armsMoveRange = 5;
    var t1 = window.setInterval(function movearms() {
        if (!me.isGameExist) {
            $("#arms" + str_value).remove();
            window.clearInterval(t1);
            return false;
        }
        if (me.isBossExist) {
            //判斷有沒有攻擊到boss
            if (bossIsAttack(str_value)) {
                me.int_monsterLPGroup = me.int_monsterLPGroup - 1;
                if (me.int_monsterLPGroup == 0) {
                    me.int_monsterLP = me.int_monsterLP - 1;
                    me.int_monsterLPGroup = 5;
                }
                if (me.int_monsterLP <= 0) {
                    $("#monsterBlood").attr("src", "sourceimg/teacher/HP/blood0.png");
                } else {
                    $("#monsterBlood").attr("src", "sourceimg/teacher/HP/blood" + me.int_monsterLP + ".png");
                }
                window.clearInterval(t1);
                return false;
            }
            //王使出大絕招 GameTime第幾關
            if (me.GameTime == 1) {//天受
                if (me.int_monsterLP <= 6) {//當王血小於等於6滴就會發出計能
                    if (!me.isBoosAttackDio) {//大招前提示只會出現一次提示
                        bossAttackInfo.frequency = 2000;
                        bossAttackInfo.moveRange = 3.5;
                        bossAttackInfo.type = "2";
                        bossAttackInfo.name = "coffee";
                        bossAttackInfo.style.height = "20%";
                        bossAttackInfo.style.width = "40%";
                        me.joinBossDio();
                        me.joinBossAttack(bossAttackInfo);
                    }
                }
            } else if (me.GameTime == 2) {//好老師
                if (me.int_monsterLP <= 7) {
                    //大招前提示只會出現一次提示
                    if (!me.isBoosAttackDio) {
                        me.joinBossDio();
                        //leave1
                        var t_boosBigAttack = window.setInterval(function boosBigAttack() {
                            if (!me.isGameExist) {
                                $("#boos1AttackDiv" + me.int_boos1SkillCount).remove();
                                window.clearInterval(t_boosBigAttack);
                                return false;
                            }
                            me.int_boos1SkillCount = me.int_boos1SkillCount + 1;
                            var moveRange = 6;//攻擊技能的移動速度

                            var boosAttackDiv = "<div id=\"boos1AttackDiv" + me.int_boos1SkillCount + "\"class=\"attack\" style=\"position:absolute;left:" + (getLeft("monsterDiv") - 100) + "px;top:" + ((getHeight("monsterDiv") / 2) + getTop("monsterDiv")) + "px;width:4%;height:4%;z-index=2\"></div>";
                            $("#start2Div").append(boosAttackDiv);
                            var boosAttack = Joe.format("<img id='boos1Attack" + me.int_boos1SkillCount + "' value='boos1AttackDiv{2}|{0}|{1}' class='attack' style='position: absolute;width:100%;height:100%;' src='sourceimg/teacher/skill/leave1.png' >", moveRange, "4", me.int_boos1SkillCount);
                            $("#boos1AttackDiv" + me.int_boos1SkillCount).append(boosAttack);
                            $("#boos1Attack" + me.int_boos1SkillCount).load(me.boosAttackMove);
                        }, 3000);
                        //leave2
                        var t_boosBigAttack2 = window.setInterval(function boosBigAttack() {
                            if (!me.isGameExist) {
                                $("#boos1AttackDiv" + me.int_boos1SkillCount).remove();
                                window.clearInterval(t_boosBigAttack2);
                                return false;
                            }
                            me.int_boos1SkillCount = me.int_boos1SkillCount + 1;
                            var moveRange = 4.5;//攻擊技能的移動速度
                            var boosAttackDiv = "<div id=\"boos1AttackDiv" + me.int_boos1SkillCount + "\"class=\"attack\" style=\"position:absolute;left:" + (getLeft("monsterDiv") - 100) + "px;top:" + ((getHeight("monsterDiv") / 2) + getTop("monsterDiv")) + "px;width:4%;height:4%;z-index=2\"></div>";
                            $("#start2Div").append(boosAttackDiv);
                            var boosAttack = Joe.format("<img id='boos1Attack" + me.int_boos1SkillCount + "' value='boos1AttackDiv{2}|{0}|{1}' class='attack' style='position: absolute;width:100%;height:100%;' src='sourceimg/teacher/skill/leave2.png' >", moveRange, "4", me.int_boos1SkillCount);
                            $("#boos1AttackDiv" + me.int_boos1SkillCount).append(boosAttack);
                            $("#boos1Attack" + me.int_boos1SkillCount).load(me.boosAttackMove);
                        }, 1000);
                    }
                }
            } else if (me.GameTime == 3) {//謝文川
                if (me.int_monsterLP <= 6) {//血6低以下時觸發攻擊
                    if (!me.isBoosAttackDio) {
                        me.joinBossDio();
                        for (var i = 1; i <= 3; i++) {
                            me.int_boos1SkillCount = me.int_boos1SkillCount + 1;
                            var moveRange = 0.5;//攻擊技能的移動速度
                            var boosAttackDiv = Joe.format("<div id=\"boos2AttackDiv{4}\"class=\"attack\" style=\"position:absolute;left:{0}px;top:{1}px;width:{2};height:{3};z-index=2\"></div>", windowWidth, windowHeight / 3 * (i - 1)+100, "7%", "14%", me.int_boos1SkillCount);
                            $("#start2Div").append(boosAttackDiv);
                            var boosAttack = Joe.format("<img id='boos2Attack" + me.int_boos1SkillCount + "' value='boos2AttackDiv{2}|{0}|{1}' class='attack' style='position: absolute;width:100%;height:100%;' src='sourceimg/teacher/skill/truck.png' >", moveRange, "3", me.int_boos1SkillCount);
                            $("#boos2AttackDiv" + me.int_boos1SkillCount).append(boosAttack);
                            $("#boos2Attack" + me.int_boos1SkillCount).load(me.boosAttackMove);
                        }



                        var t_boosBigAttack2 = window.setInterval(function boosBigAttack() {//輪子
                            if (!me.isGameExist) {
                                $("#boos1AttackDiv" + me.int_boos1SkillCount).remove();
                                window.clearInterval(t_boosBigAttack2);
                                return false;
                            }
                            me.int_boos1SkillCount = me.int_boos1SkillCount + 1;
                            var moveRange = 5;//攻擊技能的移動速度
                            var boosAttackDiv = "<div id=\"boos1AttackDiv" + me.int_boos1SkillCount + "\"class=\"attack\" style=\"position:absolute;left:" + (getLeft("monsterDiv") - 100) + "px;top:" + ((getHeight("monsterDiv") / 2) + getTop("monsterDiv")) + "px;width:5%;height:10%;z-index=2\"></div>";
                            $("#start2Div").append(boosAttackDiv);
                            var boosAttack = Joe.format("<img id='boos1Attack" + me.int_boos1SkillCount + "' value='boos1AttackDiv{2}|{0}|{1}' class='attack' style='position: absolute;width:100%;height:100%;' src='sourceimg/teacher/skill/wheel.png' >", moveRange, "6", me.int_boos1SkillCount);
                            $("#boos1AttackDiv" + me.int_boos1SkillCount).append(boosAttack);
                            $("#boos1Attack" + me.int_boos1SkillCount).load(me.boosAttackMove);

                        }, 1500);
                    }
                }

            } else if (me.GameTime == 4) {//汪老師
                if (me.int_monsterLP <= 8 && me.int_monsterLP >= 6) {
                    //大招前提示只會出現一次提示
                    if (!me.isBoosAttackDio) {
                        me.isBoosAttackDio = true;
                        var boosAttackDio = Joe.format("<img id=\"boosAttackDio\"  class=\"boosAttackDio\" style=\"width:15%;height:15%;top:{0}px;position: absolute\" src=\"sourceimg/teacher/speak/4.png\" >", getTop("monsterDiv") - getHeight("monsterDiv") / 3 * 2 + "px");
                        $("#start2Div").append(boosAttackDio);
                        $("#boosAttackDio").css({//
                            "left": getLeft("monsterDiv") - getWidth("boosAttackDio") + "px"
                        });
                        //五次問號攻擊20151115
                        var count = 0;
                        var t_boosBigAttack = window.setInterval(function boosBigAttack() {
                            me.int_boos1SkillCount = me.int_boos1SkillCount + 1;
                            if (!me.isGameExist) {
                                $("#boos1AttackDiv" + me.int_boos1SkillCount).remove();
                                window.clearInterval(t_boosBigAttack);
                                return false;
                            }
                            if (count < 3) {
                                count = count + 1;
                                var moveRange = 5;//攻擊技能的移動速度
                                var boosAttackDiv = "<div id=\"boos1AttackDiv" + me.int_boos1SkillCount + "\"class=\"attack\" style=\"position:absolute;left:" + (getLeft("monsterDiv") - 100) + "px;top:" + ((getHeight("monsterDiv") / 2) + getTop("monsterDiv")) + "px;width:40%;height:60%;z-index=2\"></div>";
                                $("#start2Div").append(boosAttackDiv);
                                var boosAttack = Joe.format("<img id='boos1Attack" + me.int_boos1SkillCount + "' value='boos1AttackDiv{2}|{0}|{1}' class='attack' style='position: absolute;width:100%;height:100%;' src='sourceimg/teacher/skill/question.png' >", moveRange, "5", me.int_boos1SkillCount);
                                $("#boos1AttackDiv" + me.int_boos1SkillCount).append(boosAttack);
                                $("#boos1Attack" + me.int_boos1SkillCount).load(me.boosAttackMove);
                            }
                        }, 1500);
                    }
                }
                if (me.int_monsterLP < 6) {//蛋蛋攻擊
                    if (!me.isBoosAttackDio2) {
                        me.isBoosAttackDio2 = true;
                        $("#boosAttackDio").attr("src", "sourceimg/teacher/speak/5.png");
                        var t_boosBigAttack2 = window.setInterval(function boosBigAttack() {
                            if (!me.isGameExist) {
                                $("#boos1AttackDiv" + me.int_boos1SkillCount).remove();
                                window.clearInterval(t_boosBigAttack2);
                                return false;
                            }
                            me.int_boos1SkillCount = me.int_boos1SkillCount + 1;
                            var moveRange = 4;//攻擊技能的移動速度
                            var boosAttackDiv = "<div id=\"boos1AttackDiv" + me.int_boos1SkillCount + "\"class=\"attack\" style=\"position:absolute;left:" + (getLeft("monsterDiv") - 100) + "px;top:" + ((getHeight("monsterDiv") / 2) + getTop("monsterDiv")) + "px;width:7%;height:14%;z-index=2\"></div>";
                            $("#start2Div").append(boosAttackDiv);
                            var boosAttack = Joe.format("<img id='boos1Attack" + me.int_boos1SkillCount + "' value='boos1AttackDiv{2}|{0}|{1}' class='attack' style='position: absolute;width:100%;height:100%;' src='sourceimg/teacher/skill/egg.png' >", moveRange, "6", me.int_boos1SkillCount);
                            $("#boos1AttackDiv" + me.int_boos1SkillCount).append(boosAttack);
                            $("#boos1Attack" + me.int_boos1SkillCount).load(me.boosAttackMove);
                        }, 2000);
                    }
                }
            }
            //判斷王死了沒
            if (me.int_monsterLP <= 0) {
                me.isVistory = true;
                me.isGameExist = false;
                $("#start2Div").remove();
                $(document).off("keydown", me.moveMain);
                $(document).off("keyup", me.starArms);
                me.endGameMessage();
            }
        }
        var armLeft = getLeft("arms" + str_value) + armsMoveRange;
        //攻擊跑出螢幕
        if (getWidth("arms" + str_value) + getLeft("arms" + str_value) > $(window).width()) {
            $("#arms" + str_value).remove();
            window.clearInterval(t1);
            return false;
        }
        $("#arms" + str_value).css({//
            "left": armLeft + "px",
            "top": armTop + "px"
        });
    }, 2);
}

//新增王的對白
GameFlow.prototype.joinBossDio = function () {
    me.isBoosAttackDio = true;
    var boosAttackDio = Joe.format("<img id=\"boosAttackDio\"  class=\"boosAttackDio\" style=\"width:15%;height:15%;top:{0}px;position: absolute\" src=\"sourceimg/teacher/speak/{1}.png\" >", getTop("monsterDiv") - getHeight("monsterDiv") / 3 * 2 + "px", me.GameTime);
    $("#start2Div").append(boosAttackDio);
    $("#boosAttackDio").css({//
        "left": getLeft("monsterDiv") - getWidth("boosAttackDio") + "px"
    });
}

//新增王的攻擊
GameFlow.prototype.joinBossAttack = function (info) {
    var t_boosBigAttack = window.setInterval(function boosBigAttack() {
        if (!me.isGameExist) {
            $("#boos1AttackDiv" + me.int_boos1SkillCount).remove();
            window.clearInterval(t_boosBigAttack);
            return false;
        }
        me.int_boos1SkillCount = me.int_boos1SkillCount + 1;
        var boosAttackDiv = Joe.format("<div id=\"boos1AttackDiv{4}\"class=\"attack\" style=\"position:absolute;left:{0}px;top:{1}px;width:{2};height:{3};z-index=2\"></div>", getLeft("monsterDiv") - 100, (getHeight("monsterDiv") / 2) + getTop("monsterDiv"), info.style.height, info.style.width, me.int_boos1SkillCount);
        $("#start2Div").append(boosAttackDiv);
        var boosAttack = Joe.format("<img id='boos1Attack{0}' value='boos1AttackDiv{0}|{1}|{2}' class='attack' style='position: absolute;width:100%;height:100%;' src='sourceimg/teacher/skill/{3}.png' >", me.int_boos1SkillCount, info.moveRange, info.type, info.name);
        $("#boos1AttackDiv" + me.int_boos1SkillCount).append(boosAttack);
        $("#boos1Attack" + me.int_boos1SkillCount).load(me.boosAttackMove);//如果加入好攻擊圖片 就讓圖片開始向主角攻擊(移動)
    }, info.frequency);

}

//移動主角
GameFlow.prototype.moveMain = function (e) {
    var keycode = e.which || e.keyCode;
    var moverange = 50;
    var mainbeeDivLeft = getLeft("mainbeeDiv");
    var mainbeeDivTop = getTop("mainbeeDiv");
    if (keycode == 37) {//left
        if (mainbeeDivLeft - moverange > 0) {
            $("#mainbeeDiv").css({
                "left": mainbeeDivLeft - moverange + "px"
            });
        }
    } else if (keycode == 39) {//right
        var lefte = mainbeeDivLeft + moverange;
        if (lefte < $(window).width() / 10 * 6) {
            $("#mainbeeDiv").css({
                "left": lefte + "px"
            });
        }
    } else if (keycode == 38) {//up
        var topp = mainbeeDivTop - moverange;
        if (topp > 0) {
            $("#mainbeeDiv").css({//
                "top": topp + "px"
            });
        }
    } else if (keycode == 40) {//down
        var topp = mainbeeDivTop + moverange;
        if (topp + moverange < $(window).height()) {
            $("#mainbeeDiv").css({//
                "top": topp + "px"
            });
        }
    }
}

//判斷有無攻擊到王
function bossIsAttack(str_value) {
    var armId = str_value;
    var bossid = "monsterDiv";
    var starr_bossleft = getLeft(bossid);
    var starr_bosstop = getTop(bossid);
    var starr_bossheight = getHeight(bossid);
    if (getWidth("arms" + armId) + getLeft("arms" + armId) > parseInt(starr_bossleft) && getHeight("arms" + armId) + getTop("arms" + armId) > parseInt(starr_bosstop) && getTop("arms" + armId) < parseInt(starr_bosstop) + parseInt(starr_bossheight)) {
        $("#arms" + armId).remove();
        return true;
    }
}

//結束遊戲刷新
GameFlow.prototype.endGame = function (e) {
    me.int_armscount = 0;//主角攻擊次數
    me.int_beeLP = 9;//主角的血量
    me.int_monsterLP = 10;//王的血量
    me.int_monsterLPGroup = 5;//一個血量要打五次
    me.int_boosAttackCount = 0;//王的攻擊次數
    me.int_littleMonsterAttackCount = 0;//小怪發射數量
    me.isGameExist = false//遊戲開始
    me.isVistory = false;
    me.isBossExist = false;
    me.GameTime = 1;//遊戲關卡歸1
    me.isBoosAttackDio = false;
    me.isBoosAttackDio2 = false;
    $(".attack").remove();
    $(".arms").remove();
    $("#endGameMessageDiv").remove();
    var audio = Joe.format("<audio autoplay loop id='menuMusic'  class='audioStart' > </audio>");
    $("#c").append(audio);
    var source = Joe.format(" <source src = 'audio/{0}.mp3' type = 'audio/mpeg'>", "menu");
    $("#menuMusic").append(source);
    $("#menu").css({
        "display": "inline"
    });
    //清除所有音檔div
    me.clearAllAudio();
}

//結算成績後遊戲刷新(繼續遊戲)
GameFlow.prototype.victory = function (e) {
    me.int_armscount = 0;//主角攻擊次數
    me.int_beeLP = 9;//主角的血量
    me.int_monsterLP = 10;//王的血量
    me.int_monsterLPGroup = 5;//一個血量要打五次
    me.int_boosAttackCount = 0;//王的攻擊次數
    me.int_littleMonsterAttackCount = 0;//小怪發射數量
    me.isGameExist = false//遊戲開始
    me.isVistory = false;
    me.isBossExist = false;
    me.isBoosAttackDio = false;
    me.isBoosAttackDio2 = false;
    $("#endGameMessageDiv").remove();
    $(".attack").remove();
    $(".arms").remove();
    me.GameTime = me.GameTime + 1;
    me.int_maxGameTime = me.GameTime;//最大關卡提升
    me.startGame();

}

//加入小怪
GameFlow.prototype.joinLittleMonster = function () {
    var me = this;
    //小怪隨機起始位置開始攻擊
    var little_MonsterStartTop = 0;
    var t_Little_MonsterAttackSpeed = 2000;//攻擊頻率
    var t_Little_Monster = window.setInterval(function t_Little_Monsterattack() {
        if (!me.isGameExist) {
            window.clearInterval(t_Little_Monster);
            return false;
        }
        var moveRange = 4;//攻擊技能的移動速度
        var type = "0";
        var winh = $(window).height();
        var maxNum = winh * 0.98;
        var minNum = 1;
        //隨機起始位置
        little_MonsterStartTop = Math.floor(Math.random() * (maxNum - minNum + 1)) + minNum;
        me.int_littleMonsterAttackCount = me.int_littleMonsterAttackCount + 1;
        // var boosAttackDiv = "<div id=\"little_MonsterDiv" + me.int_littleMonsterAttackCount + "\" class=\"little_MonsterDiv\"></div>";
        //Joe J要大寫
        var boosAttackDiv = Joe.format("<div id='little_MonsterDiv{0}' class='attack' style=\"width:4%;height:7%;left:97%;top:{1};float:left;position: absolute\" ></div>", me.int_littleMonsterAttackCount, little_MonsterStartTop);
        $("#start2Div").append(boosAttackDiv);
        var name = "";//決定用哪本書
        if (me.GameTime == "1") {
            name = "book";
        } else if (me.GameTime == "2") {
            name = "c";
        } else if (me.GameTime == "3") {
            name = "java";
        } else if (me.GameTime == "4") {
            name = "db";
        }
        var boosAttack = Joe.format("<img id='little_Monster{0}' value='little_MonsterDiv{0}|{1}|{2}' class='boosAttack' style='position: absolute;width:100%;height:100%;z-index:3' src='sourceimg/teacher/skill/{3}.png'>", me.int_littleMonsterAttackCount, moveRange, type, name);
        $("#little_MonsterDiv" + me.int_littleMonsterAttackCount).append(boosAttack);
        //增加攻擊函數
        $("#little_Monster" + me.int_littleMonsterAttackCount).load(me.boosAttackMove);
    }, t_Little_MonsterAttackSpeed);

}

//加入主角
GameFlow.prototype.joinMainBee = function () {
    var mainbeeDiv = "<div id=\"mainbeeDiv\" style=\"width:5%;height:8%;left:5%;top:5%;float:left;position: absolute\" ></div>";
    $("#start2Div").append(mainbeeDiv);
    var mainbee = "<img id=\"mainbee\"  class=\"mainbee\" style=\"width:100%;height:80%;position: absolute\" src=\"sourceimg/bee.png\" >";
    $("#mainbeeDiv").append(mainbee);
    var mainbeeBlood = "<img id=\"mainbeeBlood\"  class=\"mainbeeBlood\" style=\"width:100%;height:20%;top:80%;position: absolute\" src=\"sourceimg/player/HP/hp10.png\" >";
    $("#mainbeeDiv").append(mainbeeBlood);
}

//資訊表
GameFlow.prototype.joinInfo = function () {
    var InfoDiv = "<div id=\"InfoDiv\" style=\"width:30%;height:8%;left:5%;top:88%;float:left;position: absolute\" ></div>";
    $("#start2Div").append(InfoDiv);
    var infoBlood_src = "<img id=\"infoBlood\"  class=\"mainbeeBlood\" style=\"width:80%;height:80%;top:10%;position: absolute\" src=\"sourceimg/player/HP/hp10.png\" >";
    $("#InfoDiv").append(infoBlood_src);
    /*
    var info_blood_change = window.setInterval(function moveAttackBoos() {
        if (me.int_beeLP < 1) {
            $("#infoBlood").attr("src", "sourceimg/player/HP/hp1.png");
        } else {
            $("#infoBlood").attr("src", "sourceimg/player/HP/hp" + (me.int_beeLP + 1) + ".png");
        }
    }, 10);
    */
}

//加入王
GameFlow.prototype.joinMonster = function (bossName) {
    me.isBossExist = true;
    var monsterDiv = "<div id=\"monsterDiv\" style=\"width:20%;height:20%;left:70%;top:50%;float:left;position: absolute\" ></div>";
    $("#start2Div").append(monsterDiv);
    var monster = Joe.format("<img id=\"monster\"  class=\"monster\" style=\"width:100%;height:80%;float:left;position: absolute\" src=\"sourceimg/teacher/{0}.png\" >", bossName);
    $("#monsterDiv").append(monster);
    var monsterBlood = "<img id=\"monsterBlood\"  class=\"mainbeeBlood\" style=\"width:100%;height:20%;top:80%;position: absolute\" src=\"sourceimg/teacher/HP/blood10.png\" >";
    $("#monsterDiv").append(monsterBlood);
    var direction = 1;//控制方向
    var moveCount = 0;//移動次數
    var t_MoveBoos = window.setInterval(function moveBoos() {//boos隨機移動
        if (!me.isGameExist) {
            window.clearInterval(t_MoveBoos);
            return false;
        }
        moveCount = moveCount + 1
        var maxNum = 2;
        var minNum = 1;
        var frequency = 80;
        if (moveCount % frequency == 0) {//每10次才會更動一次方向 10*moveOneRange=一次移動的距離
            direction = Math.floor(Math.random() * (maxNum - minNum + 1)) + minNum;
        }
        var moveOneRange = 1;
        if (direction == 1) {//up
            if (getTop("monsterDiv") - moveOneRange > 0) {
                var topp = getTop("monsterDiv") - moveOneRange;
                $("#monsterDiv").css({//
                    "top": topp + "px"
                });
            } else {
                var topp = getTop("monsterDiv") + moveOneRange;
                $("#monsterDiv").css({//
                    "top": topp + "px"
                });
            }
        } else if (direction == 2) {//down
            if (getTop("monsterDiv") + getHeight("monsterDiv") + moveOneRange < $(window).height()) {
                var topp = getTop("monsterDiv") + moveOneRange;
                $("#monsterDiv").css({//
                    "top": topp + "px"
                });
            } else {
                var topp = getTop("monsterDiv") - moveOneRange;
                $("#monsterDiv").css({//
                    "top": topp + "px"
                });
            }
        }
        if (me.isBoosAttackDio) {
            $("#boosAttackDio").css({//
                "top": getTop("monsterDiv") - getHeight("monsterDiv") / 3 * 2 + "px"
            });
        }
    }, 1);

    var attackBoosSpeed = 2000;//王攻擊連發速度 一次/毫秒
    var t_AttackBoos = window.setInterval(function boosAttack() {
        if (!me.isGameExist) {
            window.clearInterval(t_AttackBoos);
            return false;
        }
        me.int_boosAttackCount = me.int_boosAttackCount + 1;
        //加入一個王攻擊的div 
        var moveRange = 5;//攻擊技能的移動速度
        var boosAttackDiv = "<div id=\"boosAttackDiv" + me.int_boosAttackCount + "\" class=\"attack\" style=\"position:absolute;left:" + (getLeft("monsterDiv") - 100) + "px;top:" + ((getHeight("monsterDiv") / 2) + getTop("monsterDiv")) + "px;width:3.5%;height:3.5%;z-index=2\"></div>";
        $("#start2Div").append(boosAttackDiv);
        if (me.GameTime == "1") {
            var boosAttack = Joe.format("<img id='boosAttack{0}' value='boosAttackDiv{0}|{1}|{2}' class='attack' style='position: absolute;width:100%;height:100%;' src='sourceimg/teacher/skill/stone.png' >", me.int_boosAttackCount, moveRange, "1");
        }
        //   var boosAttack = "<img id=\"boosAttack" + me.int_boosAttackCount + "\" value=\"boosAttackDiv" + me.int_boosAttackCount + "|" + moveRange + "\" class=\"boosAttack\" style=\"position: absolute;width:100%;height:100%;\" src=\"sourceimg/chalk.png\" >";
        $("#boosAttackDiv" + me.int_boosAttackCount).append(boosAttack);
        $("#boosAttack" + me.int_boosAttackCount).load(me.boosAttackMove);
    }, attackBoosSpeed);
}

//加入音效檔
GameFlow.prototype.joinAudio = function (id) {
    me.int_audioCount = me.int_audioCount + 1;
    var audio = Joe.format("<audio autoplay id='audioStart{0}'  class='audioStart' > </audio>", me.int_audioCount);
    $("#c").append(audio);
    var source = Joe.format(" <source src = 'audio/{0}.mp3' type = 'audio/mpeg'>", id);
    $("#audioStart" + me.int_audioCount).append(source);
}

//加入鐘聲
GameFlow.prototype.joinSchoolMusic = function (e) {
    me.int_audioCount = me.int_audioCount + 1;
    var audio = Joe.format("<audio autoplay id='audioStart{0}' class='audioStart'> </audio>", me.int_audioCount);
    $("#c").append(audio);
    var source = Joe.format(" <source src = 'audio/{0}.wav' type = 'audio/mpeg'>", "school");
    $("#audioStart" + me.int_audioCount).append(source);

    $("#audioStart" + me.int_audioCount).onended = function (e) {
        $("#audioStart" + me.int_audioCount).remove();
    };

}

//考試
examination = function () {
    //增加開場白期末考
    if (me.GameTime % 2 == 0) {
        me.joinGameDioDiv("sourceimg/level/bossBar/fin.png");
    } else {
        me.joinGameDioDiv("sourceimg/level/bossBar/mid.png");
    }

    me.joinSchoolMusic();
    me.int_audioCount = me.int_audioCount + 1;
    var windowWidth = $(window).width();
    var startGameDioSpeed = 10;
    var t_startGameDio = window.setInterval(function startGameDio() {
        var left = getLeft("startGameDio") + startGameDioSpeed;
        if (windowWidth > getLeft("startGameDio")) {
            $("#startGameDio").css({//
                "left": left + "px"
            });
        } else {
            $("#startGameDioDiv").remove();
            window.clearInterval(t_startGameDio);
            if (me.GameTime === 1) {
                me.joinMonster("sk");
            } else if (me.GameTime === 2) {
                me.joinMonster("ho")
            } else if (me.GameTime === 3) {
                me.joinMonster("sh");
            } else if (me.GameTime === 4) {
                me.joinMonster("wan");
            }
            return false;
        }
    }, startGameDioSpeed)
}

GameFlow.prototype.joinGameDioDiv = function (src) {
    var startGameDioDiv = "<div id=\"startGameDioDiv\"></div>";
    $("#start2Div").append(startGameDioDiv);
    var startGameDio = Joe.format("<img id=\"startGameDio\"  class=\"startGameDio\" style=\"left:0px;width:20%;height:50%;position: absolute;top:25%\" src=\"{0}\" >", src);
    $("#startGameDioDiv").append(startGameDio);
}

//清除音樂
GameFlow.prototype.clearAllAudio = function () {
    for (var i = 1; i <= me.int_audioCount; i++) {
        $("#audioStart" + i).remove();
    }
}

//遊戲結束刷新前畫面
GameFlow.prototype.endGameMessage = function (e) {
    me.clearAllAudio(); //清除所有音檔div
    var endGameMessageDiv = Joe.format("<div id='endGameMessageDiv'  class='endGameMessageDiv'></div>");
    $("#c").append(endGameMessageDiv);
    if (me.isVistory) {
        me.joinAudio("allpass");
        $("#endGameMessageDiv").css("background-image", "url(\"sourceimg/level/pass.png\")");
        var int_Bloodscore = (me.int_beeLP * 55) + 50;
        var int_Timescore = 0;
        var timeNumber = me.int_littleMonsterAttackCount * 2;
        if (timeNumber <= 10) {
            int_Timescore = 300;
        } else if (timeNumber <= 15) {
            int_Timescore = 250;
        } else if (timeNumber <= 20) {
            int_Timescore = 200;
        } else if (timeNumber <= 25) {
            int_Timescore = 150;
        } else if (timeNumber <= 30) {
            int_Timescore = 100;
        } else {
            int_Timescore = 50;
        }
        joinScore(int_Bloodscore, int_Timescore);
    } else {
        me.joinAudio("die");
        $("#endGameMessageDiv").css("background-image", "url(\"sourceimg/level/die.png\")");

    }
    var t = setTimeout("partialEndGame()", 3000);

}

//加入分數到畫面
joinScore = function (int_Bloodscore, int_Timescore) {
    var string_Bloodscore = int_Bloodscore.toString();
    var array_Bloodscore = new Array();
    array_Bloodscore = string_Bloodscore.split("");

    var string_Timescore = int_Timescore.toString();
    var array_Timescore = new Array();
    array_Timescore = string_Timescore.split("");

    var int_Sumscore = int_Bloodscore + int_Timescore;
    var string_Sumscore = int_Sumscore.toString();
    var array_Sumscore = new Array();
    array_Sumscore = string_Sumscore.split("");

    //加入分數-文字
    var endGameFont1 = "<img id=\"endGameFont1\"  class=\"endGameMessage\" style=\"width:14%;height:12%;float:left;top:33%;left:23%;position: absolute\" src=\"sourceimg/score/score_font_1.png\" >";
    $("#endGameMessageDiv").append(endGameFont1);
    var endGameFont2 = "<img id=\"endGameFont2\"  class=\"endGameMessage\" style=\"width:14%;height:12%;float:left;top:50%;left:23%;position: absolute\" src=\"sourceimg/score/score_font_2.png\" >";
    $("#endGameMessageDiv").append(endGameFont2);
    var endGameFont3 = "<img id=\"endGameFont3\"  class=\"endGameMessage\" style=\"width:45%;height:1%;float:left;top:68%;left:23%;position: absolute\" src=\"sourceimg/score/score_font_3.png\" >";
    $("#endGameMessageDiv").append(endGameFont3);

    //分數-數字
    for (var i = 0; i < array_Bloodscore.length; i++) {
        var left = 48 + i * 6;
        var hpScore = Joe.format("<img id='hpScore{0}'  class='hpScore' style='width:5%;height:10%;float:left;top:33%;left:{1}%;position: absolute' src='sourceimg/score/score{2}.png'>", i, left, array_Bloodscore[i]);
        $("#endGameMessageDiv").append(hpScore);
    }

    //分數-時間
    for (var i = 0; i < array_Timescore.length; i++) {
        var left = 48 + i * 6;
        var timeScore = Joe.format("<img id='timeScore{0}'  class='timeScore' style=\"width:5%;height:10%;float:left;top:50%;left:{1}%;position: absolute\" src='sourceimg/score/score{2}.png'>", i, left, array_Timescore[i]);
        $("#endGameMessageDiv").append(timeScore);
    }

    //分數-總和
    for (var i = 0; i < array_Sumscore.length; i++) {
        var left = 48 + i * 6;
        var sumScore = Joe.format("<img id='sumScore{0}'  class='sumScore' style=\"width:5%;height:10%;float:left;top:73%;left:{1}%;position: absolute\" src='sourceimg/score/score{2}.png'>", i, left, array_Sumscore[i]);
        $("#endGameMessageDiv").append(sumScore);
    }
}

partialEndGame = function () {
    var endGameMessage = "<img id=\"endGameMessage\"  class=\"endGameMessage\" style=\"width:10%;height:10%;float:left;top:80%;left:70%;position: absolute\" src=\"sourceimg/close.png\" >";
    $("#endGameMessageDiv").append(endGameMessage);

    $("#endGameMessage").css({
        cursor: "pointer"
    });

    $("#endGameMessage").hover(function () {
        $("#endGameMessage").attr("src", "sourceimg/close_touch.png");
    },
         function () {
             $("#endGameMessage").attr("src", "sourceimg/close.png");
         });
    if (me.isVistory) {//贏了最後一關
        if (me.GameTime == 4) {
            $("#endGameMessage").click(function () {
                me.proMenu();
                me.endGame();
            });
        } else {
            $("#endGameMessage").click(function () {
                me.victory();
            });
        }
    } else {
        $("#endGameMessage").click(function () {
            me.endGame();
        });
    }
}
//動態調整視窗
var windowHeight = $(window).height();
var windowWidth = $(window).width();
$("#c").height(windowHeight + "px");

function getLeft(id) {
    var str_left = $("#" + id).css("left");
    var strarr_left = str_left.split("p");
    return parseInt(strarr_left[0]);
}

function getTop(id) {
    var str_top = $("#" + id).css("top");
    var strarr_top = str_top.split("p");
    return parseInt(strarr_top[0]);
}

function getWidth(id) {
    var str_width = $("#" + id).css("width");
    var strarr_width = str_width.split("p");
    return parseInt(strarr_width[0]);
}

function getHeight(id) {
    var str_height = $("#" + id).css("height");
    var strarr_height = str_height.split("p");
    return parseInt(strarr_height[0]);
}

var obj_gameFlowSelf = new GameFlow();